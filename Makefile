all:
	test -d deps || rebar3 get-deps
	rebar3 compile
	@erl -noshell -pa './deps/bitcask/ebin' -pa './ebin' -s bertie start
